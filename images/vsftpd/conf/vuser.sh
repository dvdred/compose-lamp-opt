#!/bin/bash
set -e
DB=/etc/vsftpd/vusers_db.db
rm -f $DB
if [ ! -z "$FTP_USER" -a ! -z "$FTP_PASSWORD" ]; then
	echo -e "$FTP_USER\n$FTP_PASSWORD" | db_load -T -t hash $DB
else
	echo -e "testuser\ntestpass" | db_load -T -t hash $DB
fi
chmod 600 $DB
[ -d "$WEBROOT" ] && chown -R ftp.apache "$WEBROOT" || ( mkdir -p /var/www/ && chown ftp.apache /var/www/ )
chmod 770 /var/www
vsftpd

#!/bin/bash

# Buld instruction for private registry

ContainersPath="$PWD"
Project=${PWD##*/}

for d in $ContainersPath/*; do
    if [[ -d $d ]]; then
	cd $d
	if [[ -f "./Dockerfile" ]]; then
		Prog=${PWD##*/}
		echo "---> $PWD"
		echo "---> $Project : $Prog"
# Il seguente è un registry privato di prova
		#docker build -t "$Project/$Prog" . && docker tag -f "$Project/$Prog" registry210.cedrc.cnr.it:1500/"$Project/$Prog" && docker push registry210.cedrc.cnr.it:1500/"$Project/$Prog"
# Build in locale
		docker build -t "images/$Prog" .
	fi
    fi
    cd $ContainersPath
done

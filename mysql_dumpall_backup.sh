#!/bin/bash

Project=${PWD##*/}
# Use auth credential located in docker-compose.yml
source ./mysql_credential.env

DbIsRunning=$(docker ps | grep "$Project"_mysqlsrv_1)
[[ "x$DbIsRunning" == "x" ]] && echo 'db not running!' && exit 1

echo "Press enter to accept the default dump path or insert one:[$DBDUMP]"
read DBDUMPCST
[[ "x$DBDUMPCST" != "x" ]] && DBDUMP=$DBDUMPCST
[[ "x$DBDUMP" == "x" ]] && echo "Path is null. Aborting" && exit 1
echo "Dumping to $DBDUMP"
echo $MYSQL_ROOT_PASSWORD
docker exec -t $Project\_mysqlsrv_1 mysqldump --all-databases --password=$MYSQL_ROOT_PASSWORD > $DBDUMP/$Project\_mysqlsrv.dumpall && ls -lh $DBDUMP/$Project\_mysqlsrv.dumpall && echo "Dump of $Project database/s exported."
